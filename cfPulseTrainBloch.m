function [cf, aRLM] = cfPulseTrainBloch(aFlip, atSpoil, aFreq, aB1, aT1, maxB1)
% cfPulseTrainBloch  Cost function for Bloch simulation pulse train optimization
%
% The residual longitudinal magnetization (RLM) after a saturation pulse train
% is simulated using the Bloch equations and the cost function is defined as the
% max RLM over the aFreq x aB1 x aT1 space.  aFlip is an ordered set of flip
% angles and atSpoil are the spoiler durations. The ith value in atSpoil is the
% spoiler duration following the ith flip angle.
%
% While this may be used directly in numerical minimization, this is not
% recommended due to the potential for trapping in local minimia.  Instead,
% apply minimization to cfPulseTrainBlochPerms.m
%
% Inputs:
%  aFlip     - Ordered RF pulse flip angles (1 x n array) [deg]
%  atSpoil   - Spoiler durations (1 x n-1 array) [s]
%  aFreq     - Off-resonance (1 x m array) [Hz]
%  aB1       - B1 scaling factors (1 x p array) [ratio]
%  aT1       - Spin-lattice relaxation times (1 x q array) [s]
%  maxB1     - Maximum B1 field strength (scalar) [uT]
%
% Outputs:
%  cf        - Cost function metric
%  aRLM      - residual longitudinal magnetization, a 3D array where the
%              dimensions are [B0, B1, T1]
%
% Kelvin Chow (kelvin.chow@ualberta.ca)
% Department of Biomedical Engineering
% University of Alberta
% Revision: 1.0  Date: 29 July 2014

% Copyright (c) 2014 Kelvin Chow
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

	aRLM = nan(numel(aFreq), numel(aB1), numel(aT1));

	for i = 1:numel(aB1)
		for j = 1:numel(aT1)
			Mag = BlochSimPulseTrainMex(aFlip * aB1(i), atSpoil, aFreq, aT1(j), maxB1);

			aRLM(:,i,j) = Mag(3,end,:);
		end
	end

	cf = max(abs(aRLM(:)));
end