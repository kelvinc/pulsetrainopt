%% Saturation Pulse Train Optimization
% Saturation pulse trains can be numerically optimized for performance over
% specific ranges of B0, B1, T1, and the maximum achievable B1 field.  The
% saturation pulses optimized here are a train of hard RF pulses, all at the
% same B1 field strength, with optimized flip angles achieved by adjusting the
% length of each pulse.  The optimization evaluates all permutations of flip
% angle ordering and returns an ordered flip angle set.  This simulation uses a
% Bloch equation simulator mex function written by Brian Hargreaves, which can
% be downloaded from his website at: http://mrsrl.stanford.edu/~brian/blochsim/
%
% Kelvin Chow (kelvin.chow@ualberta.ca)
% Department of Biomedical Engineering
% University of Alberta
% Revision: 1.0  Date: 29 July 2014
%
% Copyright (c) 2014 Kelvin Chow
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

%% Setup
% Define optimization space
aB1opt15T   = linspace(0.7, 1.0,  31);       % B1 scale factor [ratio]
aB1opt3T    = linspace(0.4, 1.2,  31);
aFreqOpt15T = linspace(-120, 120, 11);       % Off-resonant frequency [Hz]
aFreqOpt3T  = linspace(-240, 240, 11);
maxB1_15T   = 0.269;                         % Maximum B1 field strength [G]
maxB1_3T    = 0.14;
aT1opt      = linspace(200, 2000, 11)/1000;  % T1 value [s]

aN = 3:6;                                    % Pulse train lengths to optimize [number of pulses]

% Inter-pulse spoiler durations [s]
atSpoilFull = [5   5.25   4.5   3.75   3.5   3   4] / 1e3; % Includes pre-spoiler
atSpoil = atSpoilFull(2:end); % Simulations don't include pre-spoiler though

% Optimization parameters
opt = optimset('fminsearch');
opt.Display = 'iter';
opt.TolX = 0.5;  % Ingore flip angle changes less than 0.5 deg for speed

% Initialize variables
cFlipTriangle15T   = cell(1,max(aN));
cFlipTriangle3T    = cell(1,max(aN));
cFlipSimple15T     = cell(1,max(aN));
cFlipSimple3T      = cell(1,max(aN));
cFlipBlochPerms15T = cell(1,max(aN));
cFlipBlochPerms3T  = cell(1,max(aN));
time15T            = nan(1,max(aN));
time3T             = nan(1,max(aN));

%% Optimization
for i = aN
	% ----- 1.5T Optimization ----------------------------------------------------
	tic

	% A. Triangle minimization algorithm
	inc = (max(aB1opt15T)-min(aB1opt15T))/i;
	a = min(aB1opt15T) + (((0:i-1)+0.5)*inc);
	cFlipTriangle15T{i} = 90./a;

	% B. Simple minimization (no B0/T1 effects)
	% Increase the resolution of the B1 space as we're not computation time limited here
	aB1opt = linspace(min(aB1opt15T), max(aB1opt15T), 200);
	cFlipSimple15T{i} = fminsearch(@(x) cfPulseTrainSimple(x, aB1opt), cFlipTriangle15T{i});

	% C. Bloch minimization with permutations
	% fminsearch returns only the unordered flip angle set
	tmp = fminsearch(@(x) cfPulseTrainBlochPerms(x, atSpoil, aFreqOpt15T, aB1opt15T, aT1opt, maxB1_15T), cFlipSimple15T{i}, opt);

	% Run cfPulseTrainBlochPerms again to determine the order
	[~, cFlipBlochPerms15T{i}] = cfPulseTrainBlochPerms(tmp, atSpoil, aFreqOpt15T, aB1opt15T, aT1opt, maxB1_15T);
	time15T(i) = toc;

	% ----- 3T Optimization ------------------------------------------------------
	tic

	% A. Triangle minimization algorithm (3T)
	inc = (max(aB1opt3T)-min(aB1opt3T))/i;
	a = min(aB1opt3T) + (((0:i-1)+0.5)*inc);
	cFlipTriangle3T{i} = 90./a;

	% B. Simple minimization (no B0/T1 effects)
	% Increase the resolution of the B1 space as we're not computation time limited here
	aB1opt = linspace(min(aB1opt3T), max(aB1opt3T), 200);
	cFlipSimple3T{i} = fminsearch(@(x) cfPulseTrainSimple(x, aB1opt), cFlipTriangle3T{i});

	% C. Bloch minimization with permutations
	% fminsearch returns only the unordered flip angle set
	tmp = fminsearch(@(x) cfPulseTrainBlochPerms(x, atSpoil, aFreqOpt3T, aB1opt3T, aT1opt, maxB1_3T), cFlipSimple3T{i}, opt);

	% Run cfPulseTrainBlochPerms again to determine the order
	[~, cFlipBlochPerms3T{i}] = cfPulseTrainBlochPerms(tmp, atSpoil, aFreqOpt3T, aB1opt3T, aT1opt, maxB1_3T);
	time3T(i) = toc;

	disp(sprintf('Simulation complete! n=%d', i))
	disp(sprintf('Elapsed Time: %1.1f+%1.1f minutes', time15T(i)/60, time3T(i)/60))
end

%% Evaluate performance for pulse train
disp(sprintf('--------------- Optimized for 1.5T ---------------'))
disp(sprintf('Optimization space:'))
disp(sprintf('  Off-resonance:   %d-%d Hz',    min(aFreqOpt15T), max(aFreqOpt15T)))
disp(sprintf('  B1 scale factor: %1.1f-%1.1f', min(aB1opt15T),   max(aB1opt15T)))
disp(sprintf('  T1 values:       %d-%d ms',    min(aT1opt)*1e3,  max(aT1opt)*1e3))
disp(sprintf('  Max B1 strength: %1.3f G',     maxB1_15T))
disp(sprintf('Results:'))

% ----- Reference train of 90s -------------------------------------------------
% disp('90-90-90!')
aSpoil = [8800 5860] / 1e6;
[~, aRLM] = cfPulseTrainBloch([90 90 90], aSpoil, aFreqOpt15T, aB1opt15T, aT1opt, pi/(4258*2*pi)*1e3);
tmp = abs(aRLM(:))*100;

[~, time] = BlochSimPulseTrainMex([90 90 90], aSpoil, aFreqOpt15T, aT1opt(1), pi/(4258*2*pi)*1e3);
duration = time(end) + (1 + 1.55)/1000;

strFlip = sprintf('%1.0f-', [90 90 90]);
disp(sprintf('  90-90-90: flip angle [deg]: %-21s  duration [ms]: %1.1f,  |Mz/M0| [%%]: %1.2f�%1.2f (%1.2f) (mean�std (max))', strFlip(1:end-1), duration*1000, mean(tmp), std(tmp), max(tmp)))

for i = aN
	% Calculate the residual longitudinal magnetization over the optimization space
	[~, aRLM] = cfPulseTrainBloch(cFlipBlochPerms15T{i}, atSpoil, aFreqOpt15T, aB1opt15T, aT1opt, maxB1_15T);
	tmp = abs(aRLM(:))*100;

	% The RF durations (and thus total duration) are dependent on the flip angle.
	% Use the Bloch simulation to get the duration
	[~, time] = BlochSimPulseTrainMex(cFlipBlochPerms15T{i}, atSpoil, aFreqOpt15T, aT1opt(1), maxB1_15T);

	% Add the pre- and post-spoiler durations
	duration = time(end) + atSpoilFull(1) + atSpoilFull(end);

	strFlip = sprintf('%1.0f-', cFlipBlochPerms15T{i});
	disp(sprintf('  %d pulses: flip angle [deg]: %-21s  duration [ms]: %1.1f,  |Mz/M0| [%%]: %1.2f�%1.2f (%1.2f) (mean�std (max))', i, strFlip(1:end-1), duration*1000, mean(tmp), std(tmp), max(tmp)))
end

disp(sprintf('--------------- Optimized for 3T ---------------'))
disp(sprintf('Optimization space:'))
disp(sprintf('  Off-resonance:   %d-%d Hz',    min(aFreqOpt3T), max(aFreqOpt3T)))
disp(sprintf('  B1 scale factor: %1.1f-%1.1f', min(aB1opt3T),   max(aB1opt3T)))
disp(sprintf('  T1 values:       %d-%d ms',    min(aT1opt)*1e3,  max(aT1opt)*1e3))
disp(sprintf('  Max B1 strength: %1.3f G',     maxB1_3T))
disp(sprintf('Results:'))

for i = aN
	% Calculate the residual longitudinal magnetization over the optimization space
	[~, aRLM] = cfPulseTrainBloch(cFlipBlochPerms3T{i}, atSpoil, aFreqOpt3T, aB1opt3T, aT1opt, maxB1_3T);
	tmp = abs(aRLM(:))*100;

	% The RF durations (and thus total duration) are dependent on the flip angle.
	% Use the Bloch simulation to get the duration
	[~, time] = BlochSimPulseTrainMex(cFlipBlochPerms3T{i}, atSpoil, aFreqOpt3T, aT1opt(1), maxB1_3T);

	% Add the pre- and post-spoiler durations
	duration = time(end) + atSpoilFull(1) + atSpoilFull(end);

	strFlip = sprintf('%1.0f-', cFlipBlochPerms3T{i});
	disp(sprintf('  %d pulses: flip angle [deg]: %-21s  duration [ms]: %1.1f,  |Mz/M0| [%%]: %1.2f�%1.2f (%1.2f) (mean�std (max))', i, strFlip(1:end-1), duration*1000, mean(tmp), std(tmp), max(tmp)))
end