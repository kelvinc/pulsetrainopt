function [Mag, time] = BlochSimPulseTrainMex(aFlip, atSpoil, aFreq, T1, maxB1, nSteps)
% BlochSimPulseTrainMex  Bloch simulation of a saturation pulse train
%
% This function performs a Bloch equation simulation of a saturation pulse train
% consisting of multiple hard RF pulses separated by spoilers.  The maximum B1
% field strength is used for all RF pulses, with flip angles altered by
% adjusting the pulse duration.  Perfect magnetization spoiling of assumed for
% each spoiler.
%
% The mex function Bloch equation simulator by Brian Hargreaves is used and must
% be compiled separately.  Source code and details can found at:
% http://www-mrsrl.stanford.edu/~brian/blochsim/
%
% See PulseTrainOptimization.m for usage examples.
%
% Inputs:
%  aFlip   - RF pulse flip angles (1 x n array) [deg]
%  atSpoil - Spoiler durations (1 x n array) [s]
%  aFreq   - Off-resonance frequencies (1 x m array) [Hz]
%  T1      - Spin-lattice relaxation time [s]
%  maxB1   - Maximum B1 field strength [G] (optional, default: 0.14)
%  nSteps  - Number of simulated steps during the RF pulse (optional, default: 1)
%
% Outputs:
%  Mag     - Magnetization following the last RF pulse (3 x t x m array), where
%            the first dimension is corresponds to [Mx My Mz] respectively
%  time    - time for Mag (1 x t array) [s]
%
% Kelvin Chow (kelvin.chow@ualberta.ca)
% Department of Biomedical Engineering
% University of Alberta
% Revision: 1.0  Date: 29 July 2014

% Copyright (c) 2014 Kelvin Chow
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

	%% Settings
	gamma = 4258;   % [Hz/G] Gyromagnetic ratio (1H)
	T2    = 0.045;  % [sec]  Myocardial T2 at 1.5T
	aPos  = 0;      % [cm]   Spatial position (single value only)
	M0    = 1;      %        Normalized starting magnetization

	if ~exist('T2', 'var') || isempty(T2) || isnan(T2)
		T2 = 0.045;  % [sec]  Myocardial T2 at 1.5T
	end
	
	% Maximum achievable B1 is limited by the RF amplifier.
	if ~exist('maxB1', 'var') || isempty(maxB1) || isnan(maxB1)
		maxB1 = 0.14 % [G]  (0.14 G = 14 uT)        Typical maximum for Siemens Skyra platform
% 		maxB1 = 0.269; % [G]  (0.269 G = 26.9 uT)  Typical maximum for Siemens Aera platform
	end

	% Number of simulated time points during each RF pulse and spoiler interval.
	% If set to 1, ignores relaxation during the RF pulse. Set to >1 if T2 is
	% relatively short compared to the pulse durations so that T2 relaxation during
	% the RF pulse is properly accounted for.
	if ~exist('nSteps', 'var') || isempty(nSteps) || isnan(nSteps)
		nSteps = 1;
	end

	% Calculate the RF durations needed to achieve the desired flip angles
	atRF = aFlip * (pi/180) / (gamma * 2*pi * maxB1); % RF durations [s]

	%% Simulation
	% Initialize Mag/time
	nTotalPts = (numel(aFlip)*2-1)*nSteps+1; % Total number of simulated time points
	Mag  = nan([3 nTotalPts numel(aFreq)]); % Dimensions: [xyz, time, freq]
	Mag(1,:,:) = 0;
	Mag(2,:,:) = 0;
	Mag(3,:,:) = M0;

	time = nan(1,nTotalPts);
	time(1) = 0;

	% Loop through each RF pulse and spoiler
	for i = 1:numel(aFlip)
		% ----- RF pulse -------------------------------------------------------------
		% Indices in time/Mag where this data is stored
		inds = (i-1)*2*nSteps + 1 + (1:nSteps);

		t  = atRF(i)/nSteps * ones(1,nSteps);
		b1 = maxB1          * ones(1,nSteps);
		gr =                 zeros(1,nSteps);

		% Input magnetization (assume perfect spoiling)
		mx0 = 0 * ones(1,numel(aFreq));
		my0 = 0 * ones(1,numel(aFreq));
		mz0 = permute(Mag(3,inds(1)-1,:), [4 3 1 2]);

		% Bloch simulation (mex)
		[mx,my,mz] = bloch(b1,gr,t,T1,T2,aFreq,aPos,2, mx0, my0, mz0); % mx/my/mz are in [time freq]
		
		% Store data into time/Mag
		Mag(1,inds,:) = mx;
		Mag(2,inds,:) = my;
		Mag(3,inds,:) = mz;
		time(inds) = time(inds(1)-1) + cumsum(t);

		% Don't simulate spoiler for last RF pulse
		if (i == numel(aFlip))
			break
		end

		% ----- Spoiler --------------------------------------------------------------
		% Indices in time/Mag where this data is stored
		inds = (i-1)*2*nSteps + 1 + (nSteps+1:2*nSteps);

		t  = atSpoil(i)/nSteps * ones(1,nSteps);
		b1 = 0                 * ones(1,nSteps);
		gr = 0                 * ones(1,nSteps);

		% Input magnetization (assume perfect spoiling)
		mx0 = 0 * ones(1,numel(aFreq));
		my0 = 0 * ones(1,numel(aFreq));
		mz0 = permute(Mag(3,inds(1)-1,:), [4 3 1 2]);

		% Bloch simulation (mex)
		[mx,my,mz] = bloch(b1,gr,t,T1,T2,aFreq,aPos,2, mx0, my0, mz0); % mx/my/mz are in [time freq]

		% Store data into time/Mag
		Mag(1,inds,:) = mx;
		Mag(2,inds,:) = my;
		Mag(3,inds,:) = mz;
		time(inds) = time(inds(1)-1) + cumsum(t);
	end
end