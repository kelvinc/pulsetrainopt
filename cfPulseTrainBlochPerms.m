function [cf, aFlipBest] = cfPulseTrainBlochPerms(aFlip, atSpoil, aFreq, aB1, aT1, maxB1)
% cfPulseTrainBlochPerms  Cost function for Bloch simulation pulse train optimization with permutations
%
% The residual longitudinal magnetization (RLM) after a saturation pulse train
% is simulated using the Bloch equations and the cost function is defined as the
% maximum RLM over the aFreq x aB1 x aT1 space.  This is evaluated for all
% permutations of the input unordered flip angle set and the lowest value from
% all permutations is returned.  atSpoil are the spoiler durations, where the
% ith value in atSpoil is the spoiler duration following the ith flip angle.
%
% See PulseTrainOptimization.m for usage examples.
%
% Inputs:
%  aFlip     - Unordered RF pulse flip angles (1 x n array) [deg]
%  atSpoil   - Spoiler durations (1 x n-1 array) [s]
%  aFreq     - Off-resonance (1 x m array) [Hz]
%  aB1       - B1 scaling factors (1 x p array) [ratio]
%  aT1       - Spin-lattice relaxation times (1 x q array) [s]
%  maxB1     - Maximum B1 field strength (scalar) [uT]
%
% Outputs:
%  cf        - Cost function metric
%  aFlipBest - Ordered flip angle array of the permutation with the lowest max
%              absolute residual longitudinal magnetization
%
% Kelvin Chow (kelvin.chow@ualberta.ca)
% Department of Biomedical Engineering
% University of Alberta
% Revision: 1.0  Date: 29 July 2014

% Copyright (c) 2014 Kelvin Chow
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

	% Generate all possible permutations of the flip angle train
	aFlipPerms = perms(aFlip);

	aMaxAbsRLM = nan(1, size(aFlipPerms,1));

	parfor i = 1:size(aFlipPerms,1)
		aMaxAbsRLM(i) = cfPulseTrainBloch(aFlipPerms(i,:), atSpoil, aFreq, aB1, aT1, maxB1);
	end

	cf = min(aMaxAbsRLM);

	if (nargout > 1)
		indBest = find(aMaxAbsRLM == min(aMaxAbsRLM), 1);
		aFlipBest = aFlipPerms(indBest,:);
	end
end