function [cf, aRLM] = cfPulseTrainSimple(aFlip, aB1)
% cfPulseTrainSimple  Cost function for simple pulse train optimization
%
% The residual longitudinal magnetization after a saturation pulse train is
% modeled as the product of the cosines of the flip angles.  This simplification
% assumes negligible relaxation during the inter-pulse spoilers and complete
% spoiling of transverse magnetization.  The residual longitudinal magnetization
% is calculated for an array of B1 scale factors and the cost function metric is
% the maximum absolute residual longitudinal magnetization over all B1s.
%
% See PulseTrainOptimization.m for usage examples.
%
% Inputs:
%  aFlip     - Ordered RF pulse flip angles (1 x n array) [deg]
%  aB1       - B1 scaling factors (1 x p array) [ratio]
%
% Outputs:
%  cf        - Cost function metric
%  aRLM      - residual longitudinal magnetization (1 x p array)
%
% Example:
%  aB1opt = linspace(0.7, 1.0, 100);  % Optimize for B1 scale factors from 0.7-1.0
%  aFlipStart = 90*ones(1,4);         % Start with a pulse train of 4 90 deg pulses
%  aFlip = fminsearch(@(x) cfPulseTrainSimple(x, aB1opt), aFlipStart);
%
% Kelvin Chow (kelvin.chow@ualberta.ca)
% Department of Biomedical Engineering
% University of Alberta
% Revision: 1.0  Date: 29 July 2014

% Copyright (c) 2014 Kelvin Chow
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

	% Make sure aFlip is a row and aB1 is a column
	if ~isrow(aFlip)
		aFlip = aFlip';
	end

	if ~iscolumn(aB1)
		aB1 = aB1';
	end

	aFlipScaled = repmat(aFlip, [numel(aB1) 1]) .* repmat(aB1, [1 numel(aFlip)]);
	aRLM = prod(cos(aFlipScaled*pi/180),2)';

	cf = mean(abs(aRLM));
end